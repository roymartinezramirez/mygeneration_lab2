package com.cynergy.mygeneration

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnVerify.setOnClickListener {
            val year = etYear.text.toString().toIntOrNull()
            if (year != null){
                when{
                    year>=2010 -> tvResult.text = getString(R.string.BabyCase)
                    year>=1994 -> tvResult.text = getString(R.string.GenerationZ)
                    year>=1981 -> tvResult.text = getString(R.string.GenerationY)
                    year>=1969 -> tvResult.text = getString(R.string.GenerationX)
                    year>=1949 -> tvResult.text = getString(R.string.GenerationBabyBoom)
                    year>=1930 -> tvResult.text = getString(R.string.SilentGeneration)
                    else -> tvResult.text = getString(R.string.OldMan)
                }
            }
        }
    }
}